# Toshokan Bug Tracker

This is the official bug tracker for [toshokan.moe](https://toshokan.moe/). Please go to [https://gitlab.com/toshokan/toshokan-bug-tracker/issues](https://gitlab.com/toshokan/toshokan-bug-tracker/issues) to view current issues and submit new ones.